const { Artboard, Rectangle, Ellipse, Text, Color } = require("scenegraph");

function rectangleCreate(selection, index) {
    let rectangle = new Rectangle();
    rectangle.width = 30 * index;
    rectangle.height = 20 * index;
    rectangle.fill = new Color("gray");
    selection.insertionParent.addChild(rectangle);
    rectangle.moveInParentCoordinates(50 * index, 50 * index);

    return rectangle;
}

function ellipseCreate(selection, index) {
    let ellipse = new Ellipse();
    ellipse.radiusX = 20 * index;
    ellipse.radiusY = 20 * index;
    ellipse.fill = new Color("#639");
    selection.insertionParent.addChild(ellipse);
    ellipse.moveInParentCoordinates(100 * index, 200 * index);

    return ellipse;
}

function textCreate(selection, index) {
    let text = new Text();
    text.text = `example text ${index}`;
    text.styleRanges = [
        {
            length: text.text.length,
            fill: new Color("#fff"),
            fontSize: 20
        }
    ];
    selection.insertionParent.addChild(text);
    text.moveInParentCoordinates(200 * index, 100 * index);
}

function createElements(selection) {
    const elements = [
        rectangleCreate, 
        rectangleCreate, 
        rectangleCreate, 
        rectangleCreate, 
        rectangleCreate, 
        ellipseCreate, 
        ellipseCreate, 
        ellipseCreate, 
        ellipseCreate, 
        ellipseCreate,
        textCreate,
        textCreate,
        textCreate,
        textCreate,
        textCreate
    ];
    
    const createdElements = elements.map((element, index) => {
        element(selection, index);
    })
    console.log('createdElements', createdElements);
}

function filterAndColor(selection, documentRoot) {
    documentRoot.children.forEach(node => {
        if (node instanceof Artboard) {
            let artboard = node;
            let rectangles = artboard.children.filter(artboardChild => {
                return artboardChild instanceof Rectangle;
            });
            rectangles.forEach(rectangle => {
                rectangle.fill = new Color("#639");
            });
        }
    })
}

module.exports = {
    commands: {
        createElements,
        filterAndColor
    }
};